﻿namespace DesktopIcons
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.btnResetIcon = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnStartStop = new System.Windows.Forms.Button();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.txtX = new System.Windows.Forms.TextBox();
            this.txtY = new System.Windows.Forms.TextBox();
            this.radioBtnGG = new System.Windows.Forms.RadioButton();
            this.radioBtnCircle = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(0, 0);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(502, 407);
            this.listBox1.TabIndex = 0;
            // 
            // btnResetIcon
            // 
            this.btnResetIcon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResetIcon.BackColor = System.Drawing.Color.Khaki;
            this.btnResetIcon.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResetIcon.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnResetIcon.Location = new System.Drawing.Point(508, 356);
            this.btnResetIcon.Name = "btnResetIcon";
            this.btnResetIcon.Size = new System.Drawing.Size(163, 43);
            this.btnResetIcon.TabIndex = 1;
            this.btnResetIcon.Text = "Reset icons";
            this.btnResetIcon.UseVisualStyleBackColor = false;
            this.btnResetIcon.Click += new System.EventHandler(this.btnResetIcon_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnStartStop
            // 
            this.btnStartStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStartStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartStop.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnStartStop.Location = new System.Drawing.Point(508, 7);
            this.btnStartStop.Name = "btnStartStop";
            this.btnStartStop.Size = new System.Drawing.Size(163, 43);
            this.btnStartStop.TabIndex = 2;
            this.btnStartStop.Text = "Start / Stop";
            this.btnStartStop.UseVisualStyleBackColor = true;
            this.btnStartStop.Click += new System.EventHandler(this.btnStartStop_Click);
            // 
            // timer2
            // 
            this.timer2.Interval = 1000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // txtX
            // 
            this.txtX.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtX.Location = new System.Drawing.Point(531, 56);
            this.txtX.Name = "txtX";
            this.txtX.Size = new System.Drawing.Size(140, 20);
            this.txtX.TabIndex = 4;
            this.txtX.Text = "2300";
            // 
            // txtY
            // 
            this.txtY.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtY.Location = new System.Drawing.Point(531, 79);
            this.txtY.Name = "txtY";
            this.txtY.Size = new System.Drawing.Size(140, 20);
            this.txtY.TabIndex = 5;
            this.txtY.Text = "540";
            // 
            // radioBtnGG
            // 
            this.radioBtnGG.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioBtnGG.AutoSize = true;
            this.radioBtnGG.Checked = true;
            this.radioBtnGG.Location = new System.Drawing.Point(508, 105);
            this.radioBtnGG.Name = "radioBtnGG";
            this.radioBtnGG.Size = new System.Drawing.Size(69, 17);
            this.radioBtnGG.TabIndex = 6;
            this.radioBtnGG.TabStop = true;
            this.radioBtnGG.Text = "Draw GG";
            this.radioBtnGG.UseVisualStyleBackColor = true;
            // 
            // radioBtnCircle
            // 
            this.radioBtnCircle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioBtnCircle.AutoSize = true;
            this.radioBtnCircle.Location = new System.Drawing.Point(508, 128);
            this.radioBtnCircle.Name = "radioBtnCircle";
            this.radioBtnCircle.Size = new System.Drawing.Size(78, 17);
            this.radioBtnCircle.TabIndex = 7;
            this.radioBtnCircle.Text = "Draw circle";
            this.radioBtnCircle.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(508, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "X:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(508, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Y:";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Location = new System.Drawing.Point(508, 148);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(163, 103);
            this.label3.TabIndex = 10;
            this.label3.Text = "Please before you run the app turn off \"Align icons to grid\" \r\n\r\nYou can do that " +
    "by going\r\n(Right Click on Desktop) > View > Align icons to grid";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.Location = new System.Drawing.Point(508, 251);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(163, 102);
            this.label4.TabIndex = 11;
            this.label4.Text = "Author:\r\nGordan Nekić\r\n\r\nSpecial thanks:\r\nNino Miškić-Pletenac";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(679, 407);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.radioBtnCircle);
            this.Controls.Add(this.radioBtnGG);
            this.Controls.Add(this.txtY);
            this.Controls.Add(this.txtX);
            this.Controls.Add(this.btnStartStop);
            this.Controls.Add(this.btnResetIcon);
            this.Controls.Add(this.listBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(695, 446);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Winicon Game";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button btnResetIcon;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnStartStop;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.TextBox txtX;
        private System.Windows.Forms.TextBox txtY;
        private System.Windows.Forms.RadioButton radioBtnGG;
        private System.Windows.Forms.RadioButton radioBtnCircle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

