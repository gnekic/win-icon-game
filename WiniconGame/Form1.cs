﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Linq;

namespace DesktopIcons
{
    public partial class Form1 : Form
    {
        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr GetDesktopWindow();

        [DllImport("user32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool EnumChildWindows(IntPtr hWndParent, EnumChildProcDelegate enumProcDelegate, IntPtr lParam);

        private delegate bool EnumChildProcDelegate(IntPtr hWnd, IntPtr lParam);

        [DllImport("user32.dll", SetLastError = true)]
        static extern int SendMessage(IntPtr hWnd, uint message, int wParam, int lParam);

        [DllImport("user32.dll", SetLastError = true)]
        static extern int SendMessage(IntPtr hWnd, uint message, IntPtr wParam, IntPtr lParam);
        [DllImport("user32.dll", SetLastError = true)]
        static extern int SendMessage(IntPtr hWnd, uint message, int wParam, StringBuilder lParam);
        [DllImport("user32.dll", SetLastError = true)]
        static extern int SendMessage(IntPtr hWnd, uint message, int wParam, IntPtr lParam);
        [DllImport("user32.dll", SetLastError = true, EntryPoint = "SendMessageA")]
        static extern IntPtr SendMessageA(IntPtr hWnd, uint message, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        static extern int GetClassName(IntPtr hWnd, StringBuilder className, int bufferSize);

        [DllImport("user32.dll")]
        public static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        public static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);
        [DllImport("user32", CharSet = CharSet.Auto, ExactSpelling = true)]
        private static extern IntPtr GetParent(IntPtr hWnd);

        const uint LVM_FIRST = 0x1000;
        const uint LVM_SETITEMPOSITION = LVM_FIRST + 15;

        const uint LVM_GETITEMCOUNT = LVM_FIRST + 0x4;
        const uint LVM_GETITEMTEXT = LVM_FIRST + 0x2d;
        const uint LVM_GETITEMPOSITION = LVM_FIRST + 16;
        const uint MEM_COMMIT = 0x1000;
        const uint MEM_RELEASE = 0x8000;
        const uint PAGE_READWRITE = 4;
        const uint PROCESS_VM_READ = 0x10;
        const uint PROCESS_VM_WRITE = 0x20;
        const uint PROCESS_VM_OPERATION = 0x8;
        const uint WM_GETTEXT = 0xd;
        const uint WM_GETTEXTLENGTH = 0xe;

        private const int GWL_STYLE = (-16);
        private const int LVS_AUTOARRANGE = 0x100;
        private const int WM_COMMAND = 0x111;
        private const int IDM_TOGGLEAUTOARRANGE = 0x7041;
        private const int IDM_TOGGLEAUTOALIGN = 0x7054;
        private const int GWL_EXSTYLE = (-20);

        public IntPtr desktopHandle;
        public IntPtr desktopListViewHandle;

        public void findDesktopAndListViewHandle() {
            // Searching for Desktop handle
            IntPtr hDesktop = GetDesktopWindow();
            Console.WriteLine(hDesktop);

            // Searching for ListView using Delegate and EnumChildWindows
            IntPtr Handle = IntPtr.Zero;
            EnumChildWindows(hDesktop,
                new EnumChildProcDelegate(delegate (IntPtr hWnd, IntPtr lParam)
                {
                    int length = SendMessage(hWnd, WM_GETTEXTLENGTH, IntPtr.Zero, IntPtr.Zero);
                    StringBuilder captionBuilder = new StringBuilder(length + 1);
                    if (length > 0)
                    {
                        int result = SendMessage(hWnd, WM_GETTEXT, captionBuilder.Capacity, captionBuilder);
                    }

                    if (captionBuilder.ToString().Equals("FolderView"))
                    {
                        Console.WriteLine(captionBuilder.ToString());
                        StringBuilder classBuilder = new StringBuilder(256);
                        int result = GetClassName(hWnd, classBuilder, classBuilder.Capacity - 1);
                        if (classBuilder.ToString().Equals("SysListView32"))
                        {
                            Handle = hWnd;
                            return false;
                        }
                    }
                    return true;
                }), IntPtr.Zero);

            // Console.WriteLine(hDesktop);
            // Console.WriteLine(Handle);
            desktopHandle = hDesktop;
            desktopListViewHandle = Handle;
        }

        List<int> initalStateOfIconsList = new List<int>();

        public void Thing() {
            RemoteListView.LVItem[] lvl = RemoteListView.GetListView("Progman", "Program Manager", // Program Window Caption and Class
                                                                "SysListView32", "FolderView"); // ListView Caption and Class
            listBox1.Items.Clear();
            string text = "";
            foreach (RemoteListView.LVItem item in lvl)
            {
                text += item.Name + " @ " + item.Location.ToString() + '\n';
                listBox1.Items.Add(item.Name + " @ " + item.Location.ToString());

                initalStateOfIconsList.Add(MakePoint(item.Location.X, item.Location.Y));
                totalCountOfIcons++;
            }

            // Save icons state.
            System.IO.File.WriteAllText(@Application.StartupPath + "\\iconLocation-" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") +".txt", text);
        }

        private int pointIndex = 0;
        private int iconIndex = 0;
        private int totalCountOfIcons = 0;

        private int MakePoint(int x, int y)
        {
            return x & 0xFFFF | y << 16;
        }

        public Form1()
        {
            InitializeComponent();
        }

        List<int> pointList = new List<int>();

        private void addCirclePoints()
        {
            int cx = Int32.Parse(txtX.Text) - 92;
            int cy = Int32.Parse(txtY.Text) - 92;
            int r = 300;
            for (double a = 0; a < 2 * Math.PI; a += 0.1)
            {
                int x = cx + (int)Math.Round(r * Math.Cos(a));
                int y = cy + (int)Math.Round(r * Math.Sin(a));
                pointList.Add(MakePoint(x, y));
            }
        }

        private void addGGPoints()
        {
            int delta = 400;

            int cx = Int32.Parse(txtX.Text) - 92;
            int cy = Int32.Parse(txtY.Text) - 92;
            int r = 300;
            for (double a = 0; a < 1.7 * Math.PI; a += 0.13)
            {
                int x = cx - delta + (int)Math.Round(r * Math.Cos(a));
                int y = cy + (int)Math.Round(r * Math.Sin(a));
                pointList.Add(MakePoint(x, y));
            }

            for (double a = 92; a < delta; a += 40)
            {
                int x = cx - (int)Math.Round(a);
                int y = cy;
                pointList.Add(MakePoint(x, y));
            }

            for (double a = 0; a < 1.7 * Math.PI; a += 0.13)
            {
                int x = cx + delta + (int)Math.Round(r * Math.Cos(a));
                int y = cy + (int)Math.Round(r * Math.Sin(a));
                pointList.Add(MakePoint(x, y));
            }

            for (double a = 92; a < delta; a += 40)
            {
                int x = cx + delta + delta - (int)Math.Round(a);
                int y = cy;
                pointList.Add(MakePoint(x, y));
            }
        }

        public Size GetCenterOfThePrimaryScreenFromIconsListOnDesktopPerspective()
        {
            
            Screen leftestScreen = null;
            Screen primaryScreen = null;
            foreach (var screen in Screen.AllScreens)
            {
                if (leftestScreen == null) { leftestScreen = screen; }
                if (primaryScreen == null) { primaryScreen = screen; }

                if (screen.Bounds.X == 0 && screen.Bounds.Y == 0) {
                    primaryScreen = screen;
                }

                if (screen.Bounds.X < leftestScreen.Bounds.X) {
                    leftestScreen = screen;
                }
                //Console.WriteLine(screen.Bounds);
            }

            Size NewSize = primaryScreen.Bounds.Size;
            NewSize.Width = (NewSize.Width / 2) + Math.Abs(leftestScreen.Bounds.X);
            NewSize.Height = (NewSize.Height / 2) + Math.Abs(leftestScreen.Bounds.Y);

            return NewSize;

            // return Screen.FromControl(this).Bounds;
            // return Screen.AllScreens.Select(screen => screen.Bounds)
            //    .Aggregate(Rectangle.Union)
            //    .Size;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            findDesktopAndListViewHandle();
            Thing();
            // addCirclePoints();
            addGGPoints();

            Size ScreenRectangle = GetCenterOfThePrimaryScreenFromIconsListOnDesktopPerspective();
            txtX.Text = (ScreenRectangle.Width).ToString();
            txtY.Text = (ScreenRectangle.Height).ToString();

            btnStartStop.Text = "Start";
            btnStartStop.BackColor = Color.Green;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            pointIndex++;
            if (pointIndex >= pointList.Count - 1) {
                pointIndex = 0;
            }

            SendMessage(desktopListViewHandle, LVM_SETITEMPOSITION, iconIndex, pointList[pointIndex]);

            if (iconIndex > totalCountOfIcons)
            {
                iconIndex = 0;
            }
            else {
                iconIndex++;
            }
            
        }

        private void btnStartStop_Click(object sender, EventArgs e)
        {
            if (timer1.Enabled == true)
            {
                timer1.Enabled = false;
                btnStartStop.Text = "Start";
                btnStartStop.BackColor = Color.Green;
            }
            else {
                pointList.Clear();
                if (radioBtnGG.Checked) {
                    addGGPoints();
                }
                if (radioBtnCircle.Checked)
                {
                    addCirclePoints();
                }
                timer1.Enabled = true;
                btnStartStop.Text = "Stop";
                btnStartStop.BackColor = Color.Red;
            }
        }

        const int WS_EX_TOPMOST = 0x00000008;

        public const int LVM_ARRANGE = 4118; // X
        public const int LVA_DEFAULT = 0x0000;
        public const int LVA_ALIGNLEFT = 0x0001;
        public const int LVA_ALIGNTOP = 0x0002;
        public const int LVA_SNAPTOGRID = 0x0005; // X

        private void btnResetIcon_Click(object sender, EventArgs e)
        {
            // Displays an OpenFileDialog so the user can select a Cursor.  
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Icons position file|*.txt";
            openFileDialog1.Title = "Select a Icons Positions File";

            // Show the Dialog.  
            // If the user clicked OK in the dialog and  
            // a .txt file was selected, open it.  
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                System.IO.StreamReader sr = new
                System.IO.StreamReader(openFileDialog1.FileName);
                string content = sr.ReadToEnd();
                sr.Close();
                string[] lines = content.Split(
                    new[] { "\n" },
                    StringSplitOptions.None
                );
                for (int i = 0; i < lines.Length; i++)
                {
                    // Define a regular expression
                    Regex rx = new Regex(@"(?<iconName>.+)\s\@\s\{X=(?<positionX>.+),Y=(?<positionY>.+)\}", RegexOptions.Compiled | RegexOptions.IgnoreCase);

                    // Define a test string.        
                    string text = lines[i];

                    // Find matches.
                    MatchCollection matches = rx.Matches(text);

                    // Report on each match.
                    foreach (Match match in matches)
                    {
                        GroupCollection groups = match.Groups;
                        Console.WriteLine("Icon {0} and X={1} Y={2}",
                                          groups["iconName"].Value,
                                          groups["positionX"].Value,
                                          groups["positionY"].Value);

                        for (int j = 0; j < listBox1.Items.Count; j++) {
                            
                            String IconMyStr = listBox1.Items[j].ToString();

                            Match singleMatch = rx.Match(IconMyStr);

                            //Console.WriteLine(singleMatch.Groups["iconName"].Value + " - " + groups["iconName"].Value);

                            if (singleMatch.Groups["iconName"].Value == groups["iconName"].Value) {
                                Console.WriteLine("Target found {0} index at J={1} but current index I={2}", singleMatch.Groups["iconName"].Value, j, i);
                                SendMessage(desktopListViewHandle, LVM_SETITEMPOSITION, j, MakePoint(Int32.Parse(groups["positionX"].Value), Int32.Parse(groups["positionY"].Value)));
                            }
                        }

                    }
                    Console.WriteLine("");
                }

            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            Console.WriteLine("Desktop:");
            Console.WriteLine(GetWindowLong(desktopHandle, GWL_STYLE));
            Console.WriteLine("Desktop ListView:");
            Console.WriteLine(GetWindowLong(desktopListViewHandle, GWL_STYLE));
        }


        [DllImport("user32.dll", SetLastError = true)] static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [DllImport("user32.dll", SetLastError = true)] static extern IntPtr GetWindow(IntPtr hWnd, GetWindow_Cmd uCmd);
        enum GetWindow_Cmd : uint
        {
            GW_HWNDFIRST = 0,
            GW_HWNDLAST = 1,
            GW_HWNDNEXT = 2,
            GW_HWNDPREV = 3,
            GW_OWNER = 4,
            GW_CHILD = 5,
            GW_ENABLEDPOPUP = 6
        }

        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool GetWindowInfo(IntPtr hwnd, ref WINDOWINFO pwi);

        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            private int _Left;
            private int _Top;
            private int _Right;
            private int _Bottom;
        }

        [StructLayout(LayoutKind.Sequential)]
        struct WINDOWINFO
        {
            public uint cbSize;
            public RECT rcWindow;
            public RECT rcClient;
            public uint dwStyle;
            public uint dwExStyle;
            public uint dwWindowStatus;
            public uint cxWindowBorders;
            public uint cyWindowBorders;
            public ushort atomWindowType;
            public ushort wCreatorVersion;

            public WINDOWINFO(Boolean? filler)
                : this()   // Allows automatic initialization of "cbSize" with "new WINDOWINFO(null/true/false)".
            {
                cbSize = (UInt32)(Marshal.SizeOf(typeof(WINDOWINFO)));
            }

        }

    }
}
